package com.mikegg.searchindex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SearchindexApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchindexApplication.class, args);
	}

}
