package com.mikegg.searchindex.resources.datasource.rv1960.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity (name = "bible_verses")
public class Rv1960Entitie {
    @Id
    private Long idVerse;
    private int idBible;
    private short idBook;
    private short chapter;
    private short verse;
    private String text;

    public Rv1960Entitie() {
    }

    public Rv1960Entitie(Long idVerse, int idBible, short idBook, short chapter, short verse, String text) {
        this.idVerse = idVerse;
        this.idBible = idBible;
        this.idBook = idBook;
        this.chapter = chapter;
        this.verse = verse;
        this.text = text;
    }

    public Long getIdVerse() {
        return idVerse;
    }

    public void setIdVerse(Long idVerse) {
        this.idVerse = idVerse;
    }

    public int getIdBible() {
        return idBible;
    }

    public void setIdBible(int idBible) {
        this.idBible = idBible;
    }

    public short getIdBook() {
        return idBook;
    }

    public void setIdBook(short idBook) {
        this.idBook = idBook;
    }

    public short getChapter() {
        return chapter;
    }

    public void setChapter(short chapter) {
        this.chapter = chapter;
    }

    public short getVerse() {
        return verse;
    }

    public void setVerse(short verse) {
        this.verse = verse;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Rv1960Entitie{" +
                "idVerse=" + idVerse +
                ", idBible=" + idBible +
                ", idBook=" + idBook +
                ", chapter=" + chapter +
                ", verse=" + verse +
                ", text='" + text + '\'' +
                '}';
    }
}
