package com.mikegg.searchindex.resources.datasource.rv1960;

import com.mikegg.searchindex.resources.datasource.rv1960.entities.Rv1960Entitie;
import org.springframework.data.repository.CrudRepository;

public interface Rv1960Db extends CrudRepository<Rv1960Entitie, Long> {
}
