package com.mikegg.searchindex.resources.repository.getbible.impl;


import com.mikegg.searchindex.resources.datasource.rv1960.Rv1960Db;
import com.mikegg.searchindex.resources.datasource.rv1960.entities.Rv1960Entitie;
import com.mikegg.searchindex.resources.repository.getbible.GetBibleRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class GetBibleRepositoryImpl implements GetBibleRepository {
    private final Rv1960Db get;

    public GetBibleRepositoryImpl(Rv1960Db get) {
        this.get = get;
    }
    @Override
    public Optional<Rv1960Entitie> getBibleRepo(Long id) {
        return get.findById(id);
    }
}
