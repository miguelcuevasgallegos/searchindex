package com.mikegg.searchindex.resources.repository.getbible;


import com.mikegg.searchindex.resources.datasource.rv1960.entities.Rv1960Entitie;

import java.util.Optional;

public interface GetBibleRepository {
    Optional<Rv1960Entitie> getBibleRepo(Long id);
}
