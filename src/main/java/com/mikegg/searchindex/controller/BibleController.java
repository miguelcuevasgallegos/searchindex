package com.mikegg.searchindex.controller;

import com.mikegg.searchindex.resources.datasource.rv1960.entities.Rv1960Entitie;
import com.mikegg.searchindex.usecases.getbibleusecase.GetBibleUseCase;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.Path;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api")
public class BibleController {
    private final GetBibleUseCase getBibleUseCase;

    public BibleController(GetBibleUseCase getBibleUseCase) {
        this.getBibleUseCase = getBibleUseCase;
    }
    @GetMapping (path = "/bible/{id}")
    public Optional<Rv1960Entitie> bible (@PathVariable Long id){
        return getBibleUseCase.getBible(id);
    }
}
