package com.mikegg.searchindex.usecases.getbibleusecase;

import com.mikegg.searchindex.resources.datasource.rv1960.entities.Rv1960Entitie;
import com.mikegg.searchindex.resources.repository.getbible.GetBibleRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GetBibleUseCase {
    private final GetBibleRepository getBibleRepository;

    public GetBibleUseCase(GetBibleRepository getBibleRepository) {
        this.getBibleRepository = getBibleRepository;
    }

    public Optional<Rv1960Entitie> getBible(Long id) {
        return getBibleRepository.getBibleRepo(id);
    }
}
